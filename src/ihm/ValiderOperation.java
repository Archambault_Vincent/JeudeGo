package ihm;
/**
 * G�re les �v�nements lord de la validation de la taille du Goban en mode 1 joueur
 */

import jeudego.*;

import java.awt.*;
import java.awt.event.*;
import Data.Player;

public class ValiderOperation implements ActionListener{
	private IhmGo go;
	public ValiderOperation(IhmGo g) {
		go=g;
	}
	public void actionPerformed(ActionEvent e) {
		//Formation of goban
		//to formate the board
		if(go.jtfSize.getText().equals("")){
			if((String)(go.bpDefine.getSelectedItem())=="19x19")
				go.size=19;
			if((String)(go.bpDefine.getSelectedItem())=="13x13")
				go.size=13;
			if((String)(go.bpDefine.getSelectedItem())=="9x9")
				go.size=9;
		}
		else{
			try{
			go.size=Integer.parseInt(go.jtfSize.getText());
			}catch(Exception erreur){
				if((String)(go.bpDefine.getSelectedItem())=="19x19")
					go.size=19;
				if((String)(go.bpDefine.getSelectedItem())=="13x13")
					go.size=13;
				if((String)(go.bpDefine.getSelectedItem())=="9x9")
					go.size=9;
			}
		}
		
		//Creation of the go's board
		go.goban=new Goban(go.size);
		go.goban.memorize(go.goban.getHistoric(),go.goban);
		try{
			//if another party upgrading buttons and panels
			go.goGame.remove(go.jpi);
			go.jpi.removeMouseListener(go.jpi.getMouseListeners()[0]);
			go.jmp.removeActionListener(go.jmp.getActionListeners()[0]);
			go.jmCancel.removeActionListener(go.jmCancel.getActionListeners()[0]);
		}catch(Exception error){}
		//Tea initialization ( graphical )
		go.jpi=new gobanIHM(go.goban);
		go.jpi.addMouseListener(new MousseOperation(go));
		go.jmp.addActionListener(new SkipTurn(go));
		go.jmCancel.addActionListener(new MoveCancelOperation(go));
		
		try{
			go.jpi.paint(go.jpi.getGraphics());
		}catch(NullPointerException npe){}
		go.goGame.add(go.jpi,BorderLayout.CENTER);
		go.goSize.setVisible(false);
		go.goSize2P.setVisible(false);
		go.goSizeCPT.setVisible(false);
		go.goGame.pack();
		go.goGame.setSize(go.jpi.getWidth()+go.vision.getWidth(), go.jpi.getHeight()+50);//50= menu size
		go.goGame.setResizable(false);
		
		//to catch the handicap
		if(go.jtfHandicapP1.getText().equals(""))
			go.handicap = 0;
		else go.handicap = Integer.parseInt(go.jtfHandicapP1.getText());
		
		if(!go.jtfj1.getText().equals(""))
			go.namep1=go.jtfj1.getText();
		if((go.bBlack.isSelected() && go.bWhite.isSelected()) || (go.bBlack.isSelected() && !go.bWhite.isSelected()) || (!go.bBlack.isSelected() && !go.bWhite.isSelected())){
			go.p1=new Player("black",go.namep1,0,true,0,true);
			go.namep2="CPT";
			go.p2=new Player("white",go.namep2,0,false,go.handicap,false);
			//go.ia=new IA(go.goban);
			go.turn=go.p1;
		}
		else {
			go.p1=new Player("black",go.namep1,0,false,0,true);
			go.namep1="CPU";
			go.p2=new Player("white",go.namep2,0,true,go.handicap,false);
			//go.ia=new IA(go.goban);
			//L'IA start
			go.turn=go.p2;
			IaOperation PlayIA=new IaOperation(go);
			PlayIA.ActionIA(go.p1,go.p2);
			go.jpi.repaint();
		}
		//Creation of the new score
		go.vision.setText("Black player : "+go.namep1+"\nWhite player : "+go.namep2);
		go.modeGo.setVisible(false);
		go.goSize.setVisible(false);
		go.goGame.setVisible(true);
		go.goGame.setEnabled(true);
	}
}
