package ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TwoPlayerOperation implements ActionListener{
	protected IhmGo go;
	public TwoPlayerOperation(IhmGo g) {
		go=g;
	}
	public void actionPerformed(ActionEvent e) {
		go.goSize2P.setVisible(true);
		go.modeGo.setEnabled(false);
		go.goSize2P.setEnabled(true);	
		go.jpChoice.setVisible(false);
	}
}