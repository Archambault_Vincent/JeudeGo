package ihm;

import jeudego.*;
import Data.*;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ValiderOperationCPT implements ActionListener, Runnable{
	private IhmGo go;
	private Thread t;
	private IaOperation PlayIA;
	private int speed;
	private boolean modeCPT;
	public ValiderOperationCPT(IhmGo g) {
		go=g;
		PlayIA = new IaOperation(go);
		speed=1000;
		modeCPT=true;
	}

	public void actionPerformed(ActionEvent arg0) {
		//to formate the board
			if(go.jtfSizeCPT.getText().equals("")){
				if((String)(go.bpDefinCPT.getSelectedItem())=="19x19")
					go.size=19;
				if((String)(go.bpDefinCPT.getSelectedItem())=="13x13")
					go.size=13;
				if((String)(go.bpDefinCPT.getSelectedItem())=="9x9")
					go.size=9;
			}
			else{
				try{
				go.size=Integer.parseInt(go.jtfSizeCPT.getText());
				}catch(Exception erreur){
					if((String)(go.bpDefinCPT.getSelectedItem())=="19x19")
						go.size=19;
					if((String)(go.bpDefinCPT.getSelectedItem())=="13x13")
						go.size=13;
					if((String)(go.bpDefinCPT.getSelectedItem())=="9x9")
						go.size=9;
				}
			}
		
		if(go.size < 6){
			go.size=6;
		}
		//creation tray
		go.goban=new Goban(go.size);
		try{
			// If another party upgrading buttons and panels
			go.goGame.remove(go.jpi);
			go.jpi.removeMouseListener(go.jpi.getMouseListeners()[0]);
			go.jmp.removeActionListener(go.jmp.getActionListeners()[0]);
			go.jmCancel.removeActionListener(go.jmCancel.getActionListeners()[0]);
		}catch(Exception error){}

		// Initialize tray ( graphical )
		go.jpi=new gobanIHM(go.goban);
	
		try{
			go.jpi.paint(go.jpi.getGraphics());
		}catch(NullPointerException npe){}
		go.goGame.add(go.jpi,BorderLayout.CENTER);
		go.goGame.pack();
		go.goGame.setSize(go.jpi.getWidth()+go.vision.getWidth(), go.jpi.getHeight()+50);//50=taille du menu
		go.goGame.setResizable(false);
		

		// Name Players Players + Training
		if(!go.jtfP1CPT.getText().equals(""))
			go.namep1=go.jtfP1CPT.getText();
		else go.namep1="Black CPU";
		go.p1=new Player ("black",go.namep1,0,false,0,true);
		if(!go.jtfP2CPT.getText().equals(""))
			go.namep2=go.jtfP2CPT.getText();
		else go.namep2="White CPU";
		//to catch handicap
		if(go.jtfHandicapCPT.getText().equals(""))
			go.handicap = 0;
		else go.handicap = Integer.parseInt(go.jtfHandicapCPT.getText());
		
		go.p2=new Player ("white",go.namep2,0,false,go.handicap,false);
		//go.ia=new IA(go.goban);
		
		//Player 1 ( Black ) begins
		go.turn=go.p1;
		//Creation of future scores
		go.vision.setText("Black's player : "+go.namep1+"\nWhite's player : "+go.namep2);
		go.modeGo.setVisible(false);
		go.goSize.setVisible(false);
		go.goSize2P.setVisible(false);
		go.goSizeCPT.setVisible(false);
		go.goGame.setVisible(true);
		go.goGame.setEnabled(true);
		go.jmNormal.setState(true);//because default speed = normal
		
		//Launch party
		modeCPT=true;//can " revive" the thread
		t=new Thread(this);
		speed=1000;
		t.start();
	}

	public void run() {
		while(!((go.p1.getPass() == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize()))) && modeCPT==true){
			try {
				Thread.sleep(speed);//unit : millisecond
			} catch (InterruptedException e){} 
			if(go.turn==go.p1)
				PlayIA.ActionIA(go.p1,go.p2);
			else PlayIA.ActionIA(go.p2,go.p1);
			go.jpi.repaint();
		}
	}
	
	public void setModeCPU(boolean b){
		modeCPT=b;
	}
	
	public void setVitesse(int vit){
		speed=vit;
	}
}