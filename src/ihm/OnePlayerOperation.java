
package ihm;

import java.awt.event.*;

public class OnePlayerOperation implements ActionListener{
	private IhmGo go;
	public OnePlayerOperation(IhmGo g) {
		go=g;
	}
	public void actionPerformed(ActionEvent arg0) {
		go.goSize.setVisible(true);
		go.modeGo.setEnabled(false);
		go.goSize.setEnabled(true);
	}
}
