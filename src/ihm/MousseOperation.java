/**
 * Lets you manage to click events on the board
 */
package ihm;

import jeudego.*;
import Data.*;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MousseOperation implements MouseListener {
	protected IhmGo go;
	private static final int SIZE_STONE = 20;
	protected PassOperation pass;
	protected gobanIHM jpi;
	protected int handicapCounter;
	public MousseOperation(IhmGo g){
		go = g;
		pass = new PassOperation(g);
	}
	public void mouseClicked(MouseEvent m) {
		if(go.handicap > 2){
			handicapCounter=go.handicap-1;
		}
		else{
			handicapCounter=go.handicap;			
		}
		Game game = new Game();
		if (m.getSource() instanceof gobanIHM){
			jpi=((gobanIHM)m.getSource());
			int i=0,h=0,x=0,y=0;
			for(x=0;x<go.size;x++){
				h=0;
				for(y=0;y<go.size;y++){
					if(Math.abs(m.getX()-(jpi.getCenterX()+i+SIZE_STONE/2))<=10 && Math.abs(m.getY()-(jpi.getCenterY()+h+SIZE_STONE/2))<=10){
						Stone p = new Stone(go.turn.getColor(),y,x,false);
						game.Turn(go.p1,go.p2,go.goban,p);
						y=go.size;
						x=go.size;
						if(go.p1.isTurn() == true ){
							go.turn=go.p1;
						}else {
							if(handicapCounter == 0){
								go.turn=go.p2;
							}else{
								go.p1.setTurn(true);
								go.p2.setTurn(false);
								go.turn=go.p1;
								go.handicap--;
							}
							
						}
					}
					//the ordinate displacement of bearing
					h=h+SIZE_STONE;
				}
				//abscissa displacement of bearing
				i=i+SIZE_STONE;
			}
			//refreshment tray
			go.jpi.repaint();
			if((go.p1.getPass() == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize()))){
				game.Turn(go.p1,go.p2,go.goban,null);
				go.jpi.removeMouseListener(go.jpi.getMouseListeners()[0]);
				go.jmp.removeActionListener(go.jmp.getActionListeners()[0]);
				go.jpi.repaint();
				pass.MessageFin(go.p1,go.p2);
				go.vision.append("\nEND OF THE GAME !!!\n"+go.p1.getName()+"score : "+go.p1.getScore() +"\n stone catch :" + go.p1.getCapture() + "\n"+go.p2.getName()+" score of : "+go.p2.getScore()+"\n" +" stone catch :" + go.p2.getCapture() + "\n");			
				}
			else{
				//IA play
				if(!go.turn.isHuman() && !(go.p1.getPass() == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize()))){
					IaOperation PlayIA = new IaOperation(go);
					if(go.p1.isHuman()==true)
					PlayIA.ActionIA(go.p2,go.p1);
					else{
					PlayIA.ActionIA(go.p1,go.p2);
					}
					go.jpi.repaint();
				}
			}
		}
		
	}
	public void mouseEntered(MouseEvent arg0) {	
	}
	public void mouseExited(MouseEvent arg0) {	
	}
	public void mousePressed(MouseEvent arg0) {	
	}
	public void mouseReleased(MouseEvent arg0) {
	}
}
