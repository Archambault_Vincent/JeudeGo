/**
 * Manages events during CPT versus CPT choice
 */
package ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CptPlayerOperation implements ActionListener{
	private IhmGo go;
	public CptPlayerOperation(IhmGo g) {
		go=g;
	}

	public void actionPerformed(ActionEvent arg0) {
		go.goSizeCPT.setVisible(true);
		go.modeGo.setEnabled(false);
		go.goSizeCPT.setEnabled(true);
	}

}
