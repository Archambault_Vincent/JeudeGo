/**
 * Manages events for the start window
 */
package ihm;

import java.awt.event.*;

class GestionDebut implements ActionListener{
	protected IhmGo go;
	public GestionDebut(IhmGo g) {
		go=g;
	}
	public void actionPerformed(ActionEvent arg0) {
			go.setVisible(false);
			go.goSize.setVisible(false);
			go.goSize2P.setVisible(false);
			go.goSizeCPT.setVisible(false);
			go.goGame.setVisible(false);
			go.modeGo.setVisible(true);
			go.modeGo.setEnabled(true);
			/* to stop for the CPT vs CPT mode thread
			 * because interrupted, stop, destroy 
			 *do not stop and thus continues even when 
			 *doing new part in another mode.
			 */
			go.gvc.setModeCPU(false);
		}
}
