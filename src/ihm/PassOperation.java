/**
 *Manages events when a player passes the turn
 *and manages the end of the game
 */
package ihm;

import jeudego.*;
import Data.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class PassOperation implements ActionListener{ 
	protected IhmGo go;
	public PassOperation(IhmGo g) {
		go=g;
	}
	public void actionPerformed(ActionEvent e) {
		if(go.turn.getColor()==go.p1.getColor()){
			go.p1.setTurn(false);
			go.vision.append("\n"+go.p1.getName()+"is spent round");
			go.turn=go.p2;
			go.vision.append("\nIt is the turn of "+go.p2.getName()+"to play");
		}
		else{
			go.p2.setTurn(true);
			go.vision.append("\n"+go.p2.getName()+" to pass his turn");
			go.turn=go.p1;
			go.vision.append("\nIt is the turn of "+go.p2.getName()+" to play");
		}
		if(go.p1.getPass() == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize())){
			go.jpi.removeMouseListener(go.jpi.getMouseListeners()[0]);
			go.jmp.removeActionListener(go.jmp.getActionListeners()[0]);
			go.jpi.repaint();
			go.goban.memorize(go.goban.getHistoric(),go.goban);;
			MessageFin(go.p1,go.p2);
			go.vision.append("\nEnd Of Game !!!\n"+go.p1.getName()+" score of : "+go.p1.getScore()+"\n"+go.p2.getName()+" a score of : "+go.p2.getScore()+"\n");
		}
		else{
			//IA play
			if(!go.turn.isHuman() && !(go.p1.getPass() == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize()))){
				IaOperation PlayIA=new IaOperation(go);
				if(go.turn==go.p1)
					PlayIA.ActionIA(go.p1,go.p2);
				else PlayIA.ActionIA(go.p2,go.p1);
			}
		}
		go.jpi.repaint();
	}
	
	/**
	 * Skip the turn of AI
	 * @param CPT player AI
	 * @param human his opponent
	 */
	public void PassIA(Player CPT, Player human){
		go.p1.setTurn(false);
		go.vision.append("\n"+go.p1.getName()+"pass his turn");
		go.turn=human;
		go.vision.append("\nIt's turn  "+go.p2.getName()+"to play");
		if((go.p1.getPass() == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize()))){
			try{
			go.jpi.removeMouseListener(go.jpi.getMouseListeners()[0]);
			go.jmp.removeActionListener(go.jmp.getActionListeners()[0]);
			}catch(Exception error){}
			go.goban.memorize(go.goban.getHistoric(),go.goban);
			go.jpi.repaint();
			MessageFin(human,CPT);
			go.vision.append("\nEnd of the game !!!\n"+human.getName()+"score of : "+human.getScore()+" n"+CPT.getName()+"score of : "+CPT.getScore()+" ");
		}
		go.jpi.repaint();
	}
	
	/**
	 *Displays the final result of the game
	 * @param p1 player 1
	 * @param p2 player 2
	 */
	public void MessageFin(Player p1,Player p2){
		Game game = new Game();
		//calculate score
		game.Turn(go.p1,go.p2,go.goban,null);
		String j="";
		//refresh mode matrix tray
		for(int i=0;i<go.size;i++){
			j=j+"\n";
			for(int h=0;h<go.size;h++)
				j=j+" "+go.goban.getGoban()[h][i].getColor();
		}
		go.tray.setText(j);
		j="";
		if(p1.getScore()>p2.getScore()){
			j=j+""+go.p1.getName()+" win the game "+(p1.getScore()-p2.getScore())+" points.";
		}
		if(p1.getScore()<p2.getScore()){
			j=j+""+go.p2.getName()+" win the game "+(p2.getScore()-p1.getScore())+" points.";
		}
		JOptionPane.showMessageDialog(go,j);
	}
}
