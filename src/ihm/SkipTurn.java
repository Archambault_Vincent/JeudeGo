package ihm;

import jeudego.*;

import Data.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class SkipTurn implements ActionListener{
	protected IhmGo go;
	public SkipTurn(IhmGo g) {
		go=g;
	}
	public void actionPerformed(ActionEvent e) {
		if(go.turn.getColor()==go.p1.getColor()){
			go.p1.setTurn(false);
			go.p2.setTurn(true);
			go.p1.setPass(go.p1.getPass()+1);
			go.vision.append("\n"+go.p1.getName()+"pass his turn");
			go.turn=go.p2;
			go.vision.append("\nIt's the turn of"+go.p2.getName()+"to play");
		}
		else{
			go.p1.setTurn(true);
			go.p2.setTurn(false);
			go.p2.setPass(go.p2.getPass()+1);
			go.vision.append("\n"+go.p2.getName()+"to pass his turn");
			go.turn=go.p1;
			go.vision.append("\nIt's the turn of"+go.p1.getName()+"to play");
		}
		if((go.p1.getPass() == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize()))){
			go.jpi.removeMouseListener(go.jpi.getMouseListeners()[0]);
			go.jmp.removeActionListener(go.jmp.getActionListeners()[0]);
			go.jpi.repaint();
			MessageFin(go.p1,go.p2);
			go.vision.append("\nEND OF THE GAME !!!\n"+go.p1.getName()+"score : "+go.p1.getScore() +"\n stone catch :" + go.p1.getCapture() + "\n"+go.p2.getName()+" score of : "+go.p2.getScore()+"\n" +" stone catch :" + go.p2.getCapture() + "\n");		
		}
		else{
			//IA play
			if(!go.turn.isHuman() && !(go.p1.getPass() == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize()))){
				IaOperation PlayIA=new IaOperation(go);
				if(go.turn==go.p1){
					PlayIA.ActionIA(go.p1,go.p2);
				}
				else{
					PlayIA.ActionIA(go.p2,go.p1);
				}
			}
		}
		go.jpi.repaint();
	}
	
	/**
	 * Spread the tower AI
	 * @param CPT player represented by AI
	 * @param human opponent
	 */
	public void PasseIA(Player CPU, Player humain){
		go.p1.setTurn(false);
		go.vision.append("\n"+go.p1.getName()+"to pass his turn");
		go.turn=humain;
		go.vision.append("\nit is the turn of "+go.p1.getName()+" to play");
		if((go.p1.getPass() == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize()))){
			try{
			go.jpi.removeMouseListener(go.jpi.getMouseListeners()[0]);
			go.jmp.removeActionListener(go.jmp.getActionListeners()[0]);
			}catch(Exception error){}
			go.jpi.repaint();
			MessageFin(humain,CPU);
			go.vision.append("\nEND OF THE GAME !!!\n"+humain.getName()+"score : "+go.p1.getScore() +"\n stone catch :" + go.p1.getCapture() + "\n"+CPU.getName()+" score of : "+go.p2.getScore()+"\n" +" stone catch :" + go.p2.getCapture() + "\n");
		}
		go.jpi.repaint();
	}
	
	/**
	 *to calculate and print the score
	 * @param p1 the player 1
	 * @param p2 the player 2
	 */
	public void MessageFin(Player p1,Player p2){
		Game game =new Game();
		game.Turn(go.p1,go.p2,go.goban,null);
		String j="";
		//refresh the matrice's mode
		for(int i=0;i<go.size;i++){
			j=j+"\n";
			for(int h=0;h<go.size;h++)
				j=j+" "+go.goban.getGoban()[h][i].getColor();
		}
		
		go.tray.setText(j);
		j="";
		if(p1.getScore()>p2.getScore()){
			j=j+""+go.p1.getName()+"win the game with "+(p1.getScore()-p2.getScore())+" points.";
		}
		if(p1.getScore()<p2.getScore()){
			j=j+""+go.p2.getName()+"win the game with "+(p2.getScore()-p1.getScore())+" points.";
		}
		JOptionPane.showMessageDialog(go,j);
	}
}

