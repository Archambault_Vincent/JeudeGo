package ihm;
/*
 * @author Cyril
 */

import java.awt.Graphics;


import jeudego.Goban;

import javax.swing.*;
import javax.swing.ImageIcon;

public class gobanIHM extends JPanel {
	
	/**
	 * To manage the pictures
	 */
	private  Goban goban; 
	private static final long serialVersionUID = 1L;
	private  int centerX = 0;
	private  int centerY = 0;
	private  int width;
	private  int height;

	private ImageIcon White = new ImageIcon("./bin/Pictures/White.gif");
	private ImageIcon Black = new ImageIcon("./bin/Pictures/Black.gif");
	private ImageIcon pWT = new ImageIcon("./bin/Pictures/pWT.gif");
	private ImageIcon pBT = new ImageIcon("./bin/Pictures/pBT.gif");
	private ImageIcon empty = new ImageIcon("./bin/Pictures/Empty.gif");
	private ImageIcon bG = new ImageIcon("./bin/Pictures/backGround.gif");
	
	private static final int SIZE_STONE = 20;

	public gobanIHM(Goban goban) {
		width = goban.getSize()*bG.getIconWidth();
		height = goban.getSize()*bG.getIconHeight();
		centerX = (width/2)-((goban.getSize()*SIZE_STONE)/2);
		centerY = (height/2)-((goban.getSize()*SIZE_STONE)/2);
		this.goban=goban;
	}
	
	public void paint(Graphics g){
		for(int i=0;i<goban.getSize();i++)
			for(int j=0;j<goban.getSize();j++)
				g.drawImage(bG.getImage(),j*bG.getIconWidth(),i*bG.getIconHeight(),null);
		for(int i=0;i<goban.getSize();i++){
			for(int j=0;j<goban.getSize  ();j++){
				switch(goban.getType()[i][j]){
				case 1 : g.drawImage(Black.getImage(),(j*SIZE_STONE)+centerX,(i*SIZE_STONE)+centerY,null);break;
				case 2: g.drawImage(White.getImage(),(j*SIZE_STONE)+centerX,(i*SIZE_STONE)+centerY,null);break;
				case -1: g.drawImage(pBT.getImage(),(j*SIZE_STONE)+centerX,(i*SIZE_STONE)+centerY,null);break;
				case -2: g.drawImage(pWT.getImage(),(j*SIZE_STONE)+centerX,(i*SIZE_STONE)+centerY,null);break;
				default: g.drawImage(empty.getImage(),(j*SIZE_STONE)+centerX,(i*SIZE_STONE)+centerY,null);break;
				}
			}
		}
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}
	
	public int getCenterX(){
		return centerX;
	}
	
	public int getCenterY(){
		return centerY;
	}
}