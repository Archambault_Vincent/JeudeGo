/**
 * Manages events when canceling the size of the Goban
 */
package ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemoveOperation implements ActionListener{
	protected IhmGo go;
	public RemoveOperation(IhmGo g) {
		go=g;
	}
	public void actionPerformed(ActionEvent e) {
		go.goSize.setVisible(false);
		go.goSize2P.setVisible(false);
		go.goSizeCPT.setVisible(false);
		go.modeGo.setVisible(true);
		go.modeGo.setEnabled(true);
	}
}
