/**
 * Manages events of a sudden cancellation
 */
package ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MoveCancelOperation implements ActionListener{
	private IhmGo go;
	public MoveCancelOperation(IhmGo g) {
		go=g;
	}
	public void actionPerformed(ActionEvent arg0) {
		//Mode P versus P
		if(go.p1.isHuman() && go.p2.isHuman()){
			boolean rewind=go.goban.Rewind(go.goban);
				go.goban.setTurn(go.goban.getTurn());	
			if(go.turn.getColor()==go.p1.getColor() && rewind == true){
				go.turn=go.p2;
				go.p1.setTurn(false);
				go.p2.setTurn(true);
			}
			else{
				go.turn=go.p1;
				go.p2.setTurn(false);
				go.p1.setTurn(true);
				}
		}
		//Mode P versus CPT
		else{
			try{
				//We cancel the stroke of CPU more than the player so that he can play again
				go.goban.Rewind(go.goban);
				go.goban.Rewind(go.goban);
				go.goban.setTurn(go.goban.getTurn());	
			}catch(Exception error){}
		}
		go.jpi.repaint();
	}

}