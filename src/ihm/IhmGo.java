package ihm;
/**
 * @author Cyril
 */
/**
 * This class it's the class to manage all constant and all the action
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

import Data.Player;
import jeudego.*;
import jeudego.Goban;

public class IhmGo extends JFrame{
	
	//The windows
	protected JFrame modeGo,goSize,goSize2P,goSizeCPT,goGame;
	
	//The panel
	protected JPanel jbStart,jpStart,jpcStart,jpiStart,jpiStart2;
	protected JPanel jpmenu,jpmStart1,jpmStart2,jpmStart3,jpmStart4,jpmStart5;
	
	//mode P versus CPT
	protected JPanel jpSize,jpDefine,jpValid,jpPlayer1,jpColor,jpChoice,jpHandicap1P;
	
	//mode P versus P
	protected JPanel jpSize2P,jpDefini2P,jpValid2P,jpPlayer12P,jpPlayer2,jpHandicap2P;
	
	//mode CPT versus CPT
	protected JPanel jpSizeCPT,jpDefinCPT,jpValidCPT,jpPlayer1CPT,jpPlayer2CPT,jpHandicapCPT;
	
	//The button
	protected JButton bStart;
	protected JButton b1P,b2P,bCPT,bHelp,bQuit;
	protected JButton bok,bCancel;
	protected JButton bok2P,bCancel2P;
	protected JButton bokCPT,bCancelCPT;
	protected JCheckBox bBlack,bWhite;
	protected JComboBox bpDefine,bpDefine2P,bpDefinCPT;
	
	//The Menus
	protected JMenuBar jmbar;
	protected JMenu jmf,jma,jmact;
	protected JMenuItem jmng,jmq,jmp,jmCancel,jmHelp,jmAb,jmS;
	protected JCheckBoxMenuItem jmSlow,jmNormal,jmFast,jmSFast;
	
	//The Labels
	protected JLabel jlCopyr,jlStart,jlTray,jlInfo,jlInfo2;
	protected JLabel s,tpd,lName1P,lcolor,lHandicap1P;
	protected JLabel s2P,tpd2P,lNameP12P,lNameP2,lHandicap2P;
	protected JLabel sCPT,tpdCPT,lNameP1CPT,lNameP2CPT,lHandicapCPT;
	
	//The text area
	protected JTextField jtfSize,jtfj1,jtfHandicapP1;
	protected JTextField jtfSize2P,jtfP12P,jtfP2,jtfHandicap2P;
	protected JTextField jtfSizeCPT,jtfP1CPT,jtfP2CPT,jtfHandicapCPT;
	
	//The Box text area
	protected JTextArea tray,vision;

	//The SCROLL PANEL
	protected JScrollPane jspvision;
	
	//The tray
	protected Goban goban;
	
	//Other
	protected String[] predefini={"19x19","13x13","9x9"};//pour la jcombobox de les tailles pr�d�fini du goban
	protected int size;//To show the size
	protected String j="";//To show the windows
	protected String namep1="player Black" ,namep2="CPT";
	protected Player p1,p2,turn;
	protected int handicap;
	protected Ia ia;
	protected static final ImageIcon start=new ImageIcon("./Picture/NEWgoban.jpg");
	protected gobanIHM jpi;
	protected ValiderOperationCPT gvc=new ValiderOperationCPT(this);//pour �tre g�rer par 3 gestions diff�rentes(Gestion debut, IHMGo et GestionVitesse)
	
	public IhmGo(String title) {
		super(title);
		
		//MAIN WINDOW
		
		//panels creation
		
		getContentPane().setLayout(new BorderLayout());//title+picture+button+copyright
		jpStart=new JPanel();
		jbStart=new JPanel();
		jpcStart=new JPanel();
		
		//Button creation
		bStart=new JButton("Start a Game");
		jlStart=new JLabel(start);
		jlCopyr=new JLabel("(c) Copyright Vincent Archambault & Cyril Belmonte & Florian Simon 2016. All rights reserved. Beta.");
		
		//Added on panels
		getContentPane().add(jpStart, BorderLayout.NORTH);
		getContentPane().add(jbStart, BorderLayout.CENTER);
		getContentPane().add(jpcStart, BorderLayout.SOUTH);
		jpStart.add(jlStart);
		jbStart.add(bStart);
		jpcStart.add(jlCopyr);
		
		//At the start
		pack();
		setResizable(false);//lock the window size
		setVisible(true);
		
		//WINDOW MENU THE NUMBER OF PLAYERS
		
		//panel creation
		modeGo=new JFrame ("Go Game");
		modeGo.getContentPane().setLayout(new GridLayout(7,1));
		jpmStart1=new JPanel();
		jpmStart2=new JPanel();
		jpmStart3=new JPanel();
		jpmStart4=new JPanel();
		jpmStart5=new JPanel();
		jpiStart=new JPanel();
		jpiStart2=new JPanel();
		
		//Button creation
		jlInfo=new JLabel("GO virtual game using the official rules");
		jlInfo2=new JLabel("(This version supports the death stones without taking)");
		b1P=new JButton("One Player match");
		b1P.setToolTipText("Play against the computer");
		b2P=new JButton("Two Player match");
		b2P.setToolTipText("Play against your friend");
		bCPT=new JButton("CPT versus CPT match");
		bCPT.setToolTipText("Observe the computer face itself");
		bHelp=new JButton("Help");
		bHelp.setToolTipText("A concern ? It's this way !");
		bQuit=new JButton("Quit");
		bQuit.setToolTipText("Quit Game of Go");
		
		//Added on panels
		modeGo.getContentPane().add(jpiStart);
		modeGo.getContentPane().add(jpiStart2);
		modeGo.getContentPane().add(jpmStart1);
		modeGo.getContentPane().add(jpmStart2);
		modeGo.getContentPane().add(jpmStart3);
		modeGo.getContentPane().add(jpmStart4);
		modeGo.getContentPane().add(jpmStart5);
		jpiStart.add(jlInfo);
		jpiStart2.add(jlInfo2);
		jpmStart1.add(b1P);
		jpmStart2.add(b2P);
		jpmStart3.add(bCPT);
		jpmStart4.add(bHelp);
		jpmStart5.add(bQuit);
		modeGo.pack();
		modeGo.setResizable(false);
		
		//WINDOW SIZE GOBAN 1P MODE
		
		//Panels creation
		goSize=new JFrame ("Go Game");
		goSize.getContentPane().setLayout(new GridLayout(7,1));
		jpHandicap1P = new JPanel();
		jpSize=new JPanel ();
		jpDefine=new JPanel ();
		jpValid=new JPanel ();
		jpPlayer1=new JPanel ();
		jpChoice=new JPanel();
		jpColor=new JPanel();
		
		//Button creation
		bok=new JButton("Validate");
		bCancel=new JButton("Canceled");
		bpDefine = new JComboBox(predefini);
		bBlack=new JCheckBox();
		bWhite=new JCheckBox();
		
		//Label creation
		s=new JLabel("Size : ");
		lHandicap1P=new JLabel("Handicap : ");
		tpd=new JLabel("Predefined : ");
		lName1P=new JLabel("Player name : ");
		lcolor=new JLabel("Choose your color : ");
		
		//text area creation
		jtfSize=new JTextField(2);
		jtfSize.setText("");
		jtfHandicapP1 = new JTextField(2);
		jtfSize.setToolTipText("The size must be a positive integer");
		jtfj1=new JTextField(8);
		jtfP2=new JTextField(8);
		
		//Added on panels
		goSize.getContentPane().add(jpSize);
		goSize.getContentPane().add(jpHandicap1P);
		goSize.getContentPane().add(jpDefine);
		goSize.getContentPane().add(jpValid);
		goSize.getContentPane().add(jpPlayer1);
		goSize.getContentPane().add(jpChoice);
		goSize.getContentPane().add(jpColor);
		jpSize.add(s);
		jpSize.add(jtfSize);
		jpHandicap1P.add(lHandicap1P);
		jpHandicap1P.add(jtfHandicapP1);
		jpDefine.add(tpd);
		jpDefine.add(bpDefine);
		jpValid.add(bok);
		jpValid.add(bCancel);
		jpPlayer1.add(lName1P);
		jpPlayer1.add(jtfj1);
		jpChoice.add(lcolor);
		jpColor.add(bBlack);
		//jpcouleur.add(new JLabel(pNoir));
		jpColor.add(bWhite);
		//jpcouleur.add(new JLabel(pBlanc));
		goSize.pack();
		goSize.setResizable(false);
		
		//FENETRE TAILLE DU GOBAN MODE 2J
		
		//Cr�ation des panels
		goSize2P=new JFrame ("GO Game");
		goSize2P.getContentPane().setLayout(new GridLayout(6,1));
		jpSize2P=new JPanel ();
		jpHandicap2P = new JPanel();
		jpDefini2P=new JPanel ();
		jpValid2P=new JPanel ();
		jpPlayer12P=new JPanel ();
		jpPlayer2=new JPanel ();
		
		//Button creation
		bok2P=new JButton("Validate");
		bCancel2P=new JButton("Cancel");
		bpDefine2P=new JComboBox(predefini);
		
		//Labels creation
		s2P=new JLabel("Size : ");
		lHandicap2P = new JLabel("Handicap : ");
		tpd2P=new JLabel("Predefined : ");
		lNameP12P=new JLabel("Black player name : ");
		lNameP2=new JLabel("White player name : ");
		
		//Text area creation
		jtfSize2P=new JTextField(2);
		jtfSize2P.setText("");
		jtfHandicap2P = new JTextField(2);
		jtfSize2P.setToolTipText("The size must be a positive integer");
		jtfP12P=new JTextField(8);
		jtfP2=new JTextField(8);
		
		//Added on panels
		goSize2P.getContentPane().add(jpSize2P);
		goSize2P.getContentPane().add(jpHandicap2P);
		goSize2P.getContentPane().add(jpDefini2P);
		goSize2P.getContentPane().add(jpValid2P);
		goSize2P.getContentPane().add(jpPlayer12P);
		goSize2P.getContentPane().add(jpPlayer2);
		jpSize2P.add(s2P);
		jpSize2P.add(jtfSize2P);
		jpHandicap2P.add(lHandicap2P);
		jpHandicap2P.add(jtfHandicap2P);
		jpDefini2P.add(tpd2P);
		jpDefini2P.add(bpDefine2P);
		jpValid2P.add(bok2P);
		jpValid2P.add(bCancel2P);
		jpPlayer12P.add(lNameP12P);
		jpPlayer12P.add(jtfP12P);
		jpPlayer2.add(lNameP2);
		jpPlayer2.add(jtfP2);
		goSize2P.pack();
		goSize2P.setResizable(false);

		//Size goban for CPT vs CPT

		//panel creation
		goSizeCPT=new JFrame ("GO Game");
		goSizeCPT.getContentPane().setLayout(new GridLayout(6,1));
		jpSizeCPT=new JPanel ();
		jpHandicapCPT = new JPanel();
		jpDefinCPT=new JPanel ();
		jpValidCPT=new JPanel ();
		jpPlayer1CPT=new JPanel ();
		jpPlayer2CPT=new JPanel ();
		
		//Buton creation
		bokCPT=new JButton("Validate");
		bCancelCPT=new JButton("Canceled");
		bpDefinCPT=new JComboBox(predefini);
		
		//label creation
		sCPT=new JLabel("Size : ");
		lHandicapCPT = new JLabel("Handicap : ");
		tpdCPT=new JLabel("Predefined : ");
		lNameP1CPT=new JLabel("Black CPT name : ");
		lNameP2CPT=new JLabel("White CPT name : ");
		
		//texte Area creating
		jtfSizeCPT=new JTextField(2);
		jtfSizeCPT.setText("");
		jtfHandicapCPT = new JTextField(2);
		jtfSizeCPT.setToolTipText("The size must be a positive integer");
		jtfP1CPT=new JTextField(8);
		jtfP2CPT=new JTextField(8);
		
		//Add on panel
		goSizeCPT.getContentPane().add(jpSizeCPT);
		goSizeCPT.getContentPane().add(jpHandicapCPT);
		goSizeCPT.getContentPane().add(jpDefinCPT);
		goSizeCPT.getContentPane().add(jpValidCPT);
		goSizeCPT.getContentPane().add(jpPlayer1CPT);
		goSizeCPT.getContentPane().add(jpPlayer2CPT);
		jpSizeCPT.add(sCPT);
		jpSizeCPT.add(jtfSizeCPT);
		jpHandicapCPT.add(lHandicapCPT);
		jpHandicapCPT.add(jtfHandicapCPT);
		jpDefinCPT.add(tpdCPT);
		jpDefinCPT.add(bpDefinCPT);
		jpValidCPT.add(bokCPT);
		jpValidCPT.add(bCancelCPT);
		jpPlayer1CPT.add(lNameP1CPT);
		jpPlayer1CPT.add(jtfP1CPT);
		jpPlayer2CPT.add(lNameP2CPT);
		jpPlayer2CPT.add(jtfP2CPT);
		goSizeCPT.pack();
		goSizeCPT.setResizable(false);
		
		//WINDOW GO GAME
		
		//Panel creation
		goGame=new JFrame("Jeu de GO");
		goGame.setLayout(new BorderLayout());
		
		//Menu creation
		jmbar=new JMenuBar();
		jmf=new JMenu("Files");
		jmf.setMnemonic(KeyEvent.VK_F);
		jmng=new JMenuItem("New game",KeyEvent.VK_N);
		jmq=new JMenuItem("Quit",KeyEvent.VK_Q);
		jmact=new JMenu("Action");
		jmact.setMnemonic(KeyEvent.VK_C);
		jmp=new JMenuItem("Skip turn",KeyEvent.VK_P);
		jmCancel=new JMenuItem("Cancel the move",KeyEvent.VK_U);
		jmS=new JMenu("CPT speed");
		jmS.setMnemonic(KeyEvent.VK_V);
		jmSlow=new JCheckBoxMenuItem("Slow");
		jmNormal=new JCheckBoxMenuItem("Normal");
		jmFast=new JCheckBoxMenuItem("Fast");
		jmSFast=new JCheckBoxMenuItem("Very Fast");
		jma=new JMenu("Help");
		jma.setMnemonic(KeyEvent.VK_A);
		jmHelp=new JMenuItem("Help Manual",KeyEvent.VK_M);
		jmAb=new JMenuItem("About...",KeyEvent.VK_P);
		
		//shortened and submenus creation
		jmng.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.ALT_MASK));//Raccourci clavier(Alt+N)
		jmq.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,ActionEvent.ALT_MASK));
		jmp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.ALT_MASK));
		jmCancel.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.ALT_MASK));
		jmHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.ALT_MASK));

		//text area creation
		tray=new JTextArea(10,10);
		tray.setEditable(false);
		vision=new JTextArea(10,10);
		vision.setEditable(false);
		vision.setLineWrap(true);	   // an automatic online return is desired
		vision.setWrapStyleWord(true); // One wishes that the words are not cut

		//Creation of scroll panels
		jspvision=new JScrollPane(vision);
	
		//Added on panels
		goGame.setJMenuBar(jmbar);
		jmf.add(jmng);
		jmf.addSeparator();
		jmf.add(jmq);
		jmact.add(jmp);
		jmbar.add(jmf);
		jmact.add(jmCancel);
		jmact.addSeparator();
		jmact.addSeparator();
		jmS.add(jmSlow);
		jmS.add(jmNormal);
		jmS.add(jmFast);
		jmS.add(jmSFast);
		jmact.add(jmS);
		jmbar.add(jmact);
		jma.add(jmHelp);
		jma.add(jmAb);
		jmbar.add(jma);
		goGame.add(jspvision,BorderLayout.EAST);
		goGame.pack();
		
		//Center the windows on the screen
		setLocationRelativeTo(null);
		modeGo.setLocationRelativeTo(null);
		goSize.setLocationRelativeTo(null);
		goSize2P.setLocationRelativeTo(null);
		goSizeCPT.setLocationRelativeTo(null);
		goGame.setLocationRelativeTo(null);
		
		//additions of detector buttons (go game)
		jmq.addActionListener(new ExitOperation(this));
		jmng.addActionListener(new GestionDebut(this));
		tray.addMouseListener(new MousseOperation(this));
		jmHelp.addActionListener(new HelpOperation());
		bHelp.addActionListener(new HelpOperation());
		jmAb.addActionListener(new HistoryOperation());
		jmSlow.addActionListener(new SpeedOperation(gvc,this));
		jmNormal.addActionListener(new SpeedOperation(gvc,this));
		jmFast.addActionListener(new SpeedOperation(gvc,this));
		jmSFast.addActionListener(new SpeedOperation(gvc,this));
		
		goGame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener (new WindowAdapter(){
			public void windowClosing (WindowEvent e){
				System.exit(0);
			}
		});
		
		//additions of detector buttons (size go)
		bok.addActionListener(new ValiderOperation(this));
		bok2P.addActionListener(new Valider2POperation(this));
		bokCPT.addActionListener(gvc);
		bCancel.addActionListener(new RemoveOperation(this));
		bCancel2P.addActionListener(new RemoveOperation(this));
		bCancelCPT.addActionListener(new RemoveOperation(this));
		goSize.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener (new WindowAdapter(){
			public void windowClosing (WindowEvent e){
				System.exit(0);
			}
		});
		
		//additions of detector buttons (player menu)
		b1P.addActionListener(new OnePlayerOperation(this));
		b2P.addActionListener(new TwoPlayerOperation(this));
		bCPT.addActionListener(new CptPlayerOperation(this));
		bQuit.addActionListener(new ExitOperation(this));
		modeGo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener (new WindowAdapter(){
			public void windowClosing (WindowEvent e){
				System.exit(0);
			}
		});
		
		//additions of detector buttons (start)
		bStart.addActionListener(new GestionDebut(this));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addWindowListener (new WindowAdapter(){
			public void windowClosing (WindowEvent e){
				System.exit(0);
			}
		});
	}		
	public static void main(String args[]){
    	new IhmGo("Go Game");
    }
}