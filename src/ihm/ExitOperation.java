/**
 * Manages events when leaving the game.
 */
package ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class ExitOperation implements ActionListener{
	protected IhmGo go;
	public ExitOperation(IhmGo g) {
		go=g;
	}
	public void actionPerformed(ActionEvent e) {
		sortieAvecConfirmation ();
	}
	//Confirmation of " Quit Game "
	void sortieAvecConfirmation (){
		if (JOptionPane.showConfirmDialog (go, 
		"Do you really want to quit?",
		"Confirmation request", 
		JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
			System.exit(0);
		}
	}
}