/**
 *Manage ai action
 **/
package ihm;

import jeudego.*;
import Data.*;


public class IaOperation{
	private IhmGo go;
	private PassOperation pass;
	private int counter;
	protected int handicapCounter;
	public IaOperation(IhmGo g) {
		go=g;
		pass =new PassOperation(g);
		counter=0;
	}
	
	/**
	 * the action 
	 * @param CPT Ia player's
	 * @param human player's
	 */
	public void ActionIA(Player CPT, Player human) {
		Game game = new Game();
			if(!(((go.p1.getPass()+go.p2.getPass()) == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize())))){
				game.Turn(CPT,human, go.goban,null);
				go.vision.append("\n"+go.p1.getName()+" played");
				go.turn=human;
				go.vision.append("\nIt's the turn "+go.p2.getName()+"to play");
				
			}else {
				if(((go.p1.getPass()+go.p2.getPass()) == 2 || go.goban.Full(go.goban.getType(), go.goban.getSize()))){
					try{
						go.jpi.removeMouseListener(go.jpi.getMouseListeners()[0]);
						go.jmp.removeActionListener(go.jmp.getActionListeners()[0]);
						go.jpi.repaint();
					}catch (Exception error){}
					pass.MessageFin(CPT,human);
					go.vision.append("\nEND OF THE GAME !!!\n"+go.p1.getName()+"score : "+go.p1.getScore() +"\n stone catch :" + go.p1.getCapture() + "\n"+go.p2.getName()+" score of : "+go.p2.getScore()+"\n" +" stone catch :" + go.p2.getCapture() + "\n");		
					}
				else{
					go.vision.append("\n"+go.p2.getName()+" replays");
					//IA replay
					//Tip to avoid a bug in a rare situation that could not be corrected
					if(counter<=20){
					counter++;
					ActionIA(CPT,human);
					}
					else pass.PassIA(CPT,human);
				}
			
		}
	}
}
