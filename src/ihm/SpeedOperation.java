package ihm;
/**
 * This class manage the speed of the CPT player
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SpeedOperation implements ActionListener {
	private ValiderOperationCPT cc;
	private IhmGo go;
	public SpeedOperation(ValiderOperationCPT c,IhmGo g) {
		cc=c;
		go=g;
	}

	public void actionPerformed(ActionEvent click) {
		
		if(click.getSource()==go.jmSlow){
			go.jmSlow.setState(true);
			go.jmNormal.setState(false);
			go.jmFast.setState(false);
			go.jmSFast.setState(false);
			cc.setVitesse(1000);
		}
		if(click.getSource()==go.jmNormal){
			go.jmSlow.setState(false);
			go.jmNormal.setState(true);
			go.jmFast.setState(false);
			go.jmSFast.setState(false);
			cc.setVitesse(500);
		}
		if(click.getSource()==go.jmFast){
			go.jmSlow.setState(false);
			go.jmNormal.setState(false);
			go.jmFast.setState(true);
			go.jmSFast.setState(false);
			cc.setVitesse(250);
		}
		if(click.getSource()==go.jmSFast){
			go.jmSlow.setState(false);
			go.jmNormal.setState(false);
			go.jmFast.setState(false);
			go.jmSFast.setState(true);
			cc.setVitesse(1);
		}
	}

}
