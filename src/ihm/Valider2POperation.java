/**
 * G�re les �v�nements lord de la validation de la taille du Goban en mode 2 joueurs
 */
package ihm;

import jeudego.*;
import Data.*;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Valider2POperation implements ActionListener{
	private IhmGo go;
	public Valider2POperation(IhmGo g) {
		go=g;
	}
	public void actionPerformed(ActionEvent arg0) {
		//to formate the board
			if(go.jtfSize2P.getText().equals("")){
				if((String)(go.bpDefine2P.getSelectedItem())=="19x19")
					go.size=19;
				if((String)(go.bpDefine2P.getSelectedItem())=="13x13")
					go.size=13;
				if((String)(go.bpDefine2P.getSelectedItem())=="9x9")
					go.size=9;
			}
			else{
				try{
				go.size=Integer.parseInt(go.jtfSize2P.getText());
				}catch(Exception erreur){
					if((String)(go.bpDefine2P.getSelectedItem())=="19x19")
						go.size=19;
					if((String)(go.bpDefine2P.getSelectedItem())=="13x13")
						go.size=13;
					if((String)(go.bpDefine2P.getSelectedItem())=="9x9")
						go.size=9;
				}
			}
		
		//Creation of the board
		go.goban=new Goban(go.size);
		go.goban.memorize(go.goban.getHistoric(),go.goban);
		try{
			//if another party upgrading buttons and panels
			go.goGame.remove(go.jpi);
			go.jpi.removeMouseListener(go.jpi.getMouseListeners()[0]);
			go.jmp.removeActionListener(go.jmp.getActionListeners()[0]);
			go.jmCancel.removeActionListener(go.jmCancel.getActionListeners()[0]);
		}catch(Exception error){}
		//Tea initialization ( graphical )
		go.jpi=new gobanIHM(go.goban);
		go.jpi.addMouseListener(new MousseOperation(go));
		go.jmp.addActionListener(new SkipTurn(go));
		go.jmCancel.addActionListener(new MoveCancelOperation(go));
		
		try{
			go.jpi.paint(go.jpi.getGraphics());
		}catch(NullPointerException npe){}
		go.goSize.setVisible(false);
		go.goSize2P.setVisible(false);
		go.goSizeCPT.setVisible(false);
		go.goGame.add(go.jpi,BorderLayout.CENTER);
		go.goGame.pack();
		go.goGame.setSize(go.jpi.getWidth()+go.vision.getWidth(), go.jpi.getHeight()+50);//50=taille du menu
		go.goGame.setResizable(false);
		
		//to catch the handicap
		if(go.jtfHandicap2P.getText().equals(""))
			go.handicap = 0;
		else go.handicap = Integer.parseInt(go.jtfHandicap2P.getText());
		
		//Name Players Players + Training
		if(!go.jtfP12P.getText().equals(""))
			go.namep1=go.jtfP12P.getText();
		else go.namep1="Black player";
		go.p1=new Player("black",go.namep1,0,true,0,true);
		if(!go.jtfP2.getText().equals(""))
			go.namep2=go.jtfP2.getText();
		else go.namep2="White player";
		go.p2=new Player ("white",go.namep2,0,true,go.handicap,false);
		
		//Player 1 ( Black ) begins
		go.turn=go.p1;
		go.vision.setText("Black player : "+go.namep1+"\nWhite player : "+go.namep2);
		go.modeGo.setVisible(false);
		go.goSize.setVisible(false);
		go.goGame.setVisible(true);
		go.goGame.setEnabled(true);
	}

}
