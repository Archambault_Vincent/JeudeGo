/**
 * To manages the "About..
 * 
 **/
package ihm;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class HistoryOperation implements ActionListener{
	private JFrame about;
	private JLabel img;
	private JPanel jpok;
	private JTextArea ab;
	private JButton ok;
	private static final ImageIcon geisha=new ImageIcon("img/geisha.jpg");
	
	public HistoryOperation() {
		about=new JFrame("About the Go");
		about.getContentPane().setLayout(new BorderLayout());
		img=new JLabel(geisha);
		about.getContentPane().add(img, BorderLayout.WEST);
		ab=new JTextArea("Go Game\nVersion 1.0\n(c) Copyright Vincent Archambault & Cyril Belmonte et Florian Simon 2007.\nAll rights reserved.");
		ab.setEditable(false);
		ab.setLineWrap(true); 
		ab.setWrapStyleWord(true);
		about.getContentPane().add(ab, BorderLayout.EAST);
		ok=new JButton("Ok");
		ok.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				about.setVisible(false);
			}
		});
		jpok=new JPanel(new FlowLayout());
		jpok.add(ok);
		about.getContentPane().add(jpok, BorderLayout.SOUTH);
		about.pack();
		about.setLocationRelativeTo(null);
		about.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		about.addWindowListener (new WindowAdapter(){
			public void windowClosing (WindowEvent e){
				about.setVisible(false);
			}
		});
	}
	
	public void actionPerformed(ActionEvent arg0){
		about.setVisible(true);
	}

}