/**
 * @author Vincent Archambault
 */
package jeudego;
import Data.Player;
import Data.Stone;
public class Capture {
	/**
	 * Initialisation marqueur
	 * @param marqueur is a table for Deathchain  
	 * @param size is the goban size
	 */
	public void Initmarqueur(boolean[][] marqueur,int size){
		int index;
		int index2;
		for(index=0 ; index < size-1 ; index++){
			for(index2=0 ; index2 < size-1 ; index2++){
			marqueur[index][index2]=false;
			}
		}
	}	
	/**
	 *say if a Stone is surrounded
	 *@param goban is the goban
	 *@param type is the type of case  
	 *@param x is the axisX
	 *@param y is the axisY
	 *@param size is the goban size
	 *@return true if the stone is surrounded
	 *@return false if it have a free side next him
	 */
	public boolean Dead(Stone[][] goban ,int[][] type,int x, int y,int size){
		//check number of free case
		int free=Freestone(type, x, y, size);
		if (free == 4){
			//say if the stone can be kill
			goban[x][y].setDead(true);
			return	true;
			}
		else{
			return false;
		}
	}	
	/**
	 *check if they are more than one stone dead
	 *@param type is the type of case  
	 *@param stone is the stone catched 
	 *@param size is the goban size
	 *@param marqueur check if the stone has been visited 
	 *@return true if no stone are free
	 *@return false if one stone is free
	 */
	public boolean Deathchain (int[][] type ,Stone stone, int size,Stone[][] goban,boolean[][] marqueur){
		//stone has been visited 
		marqueur[stone.getAxisX()][stone.getAxisY()]=true;
		//check x+1 side
		if(stone.getAxisX() > 0){
			Stone stonenext=goban[stone.getAxisX()-1][stone.getAxisY()];
			if(stonenext.getColor().equals(stone.getColor())==true && marqueur[stonenext.getAxisX()][stonenext.getAxisY()]==false){
				//say if the stone is dead or not and continue 
				if(Dead(goban,type,stonenext.getAxisX(),stonenext.getAxisY(), size)==false || Deathchain (type ,stonenext,size,goban,marqueur)==false){;
				return false;
				}
			}
		}
		//check y+1 side
		if(stone.getAxisY() > 0){
			Stone stonenext=goban[stone.getAxisX()][stone.getAxisY()-1];
			if(stonenext.getColor().equals(stone.getColor())==true && marqueur[stonenext.getAxisX()][stonenext.getAxisY()]==false){
				//say if the stone is dead or not and continue 
				if(Dead(goban,type,stonenext.getAxisX(),stonenext.getAxisY(), size)==false || Deathchain (type ,stonenext,size,goban,marqueur)==false){;
				return false;
					}
				}
			}
		//check x-1 side
		if(stone.getAxisX() < size-1){
			Stone stonenext=goban[stone.getAxisX()+1][stone.getAxisY()];
			if(stonenext.getColor().equals(stone.getColor())==true && marqueur[stonenext.getAxisX()][stonenext.getAxisY()]==false){
				//say if the stone is dead or not and continue 
				if(Dead(goban,type,stonenext.getAxisX(),stonenext.getAxisY(), size)==false || Deathchain (type ,stonenext,size,goban,marqueur)==false){;
					return false;
				}
			}
		}
		//check y-1 side
		if(stone.getAxisY() < size-1){
			Stone stonenext=goban[stone.getAxisX()][stone.getAxisY()+1];
			if(stonenext.getColor().equals(stone.getColor())==true && marqueur[stonenext.getAxisX()][stonenext.getAxisY()]==false){
				//say if the stone is dead or not and continue 
				if(Dead(goban,type,stonenext.getAxisX(),stonenext.getAxisY(), size)==false || Deathchain (type ,stonenext,size,goban,marqueur)==false){;
					return false;
					}
				}
			}
		return true;
	}
	/**
	 *calculated number of free side
	 *@param type is the type of case  
	 *@param x is the axisX
	 *@param y is the axisY
	 *@param size is the goban size
	 *@return number of free side
	 */
	public int Freestone (int[][] type , int x , int y ,int size){
		int free=0;
		//case axisX+1
		if(x < size-1){
			if(type[x+1][y] > 0){
				free++;
				}
			}
		else{
			free++;
		}		
		//case axisX-1
		if(x > 0){
			if(type[x-1][y] > 0){
				free++;
				}
			}
		else{
			free++;
		}		
		//case axisY+1
		if(y < size-1){	
			if(type[x][y+1] > 0){
				free++;
				}
			}
		else{
			free++;
		}		
		//case axisY-1
		if(y > 0){
			if(type[x][y-1] > 0){
				free++;
				}
			}
		else{
			free++;
		}		
	return free;	
	}
/**
 * remove stone destroy
 * @param type type of the case
 * @param size size of the goban 
 * @param goban goban is the goban
 * @param player the playter who played
 * @param ko boolean if is a simulation or not
 */
	public void Destroy (int[][] type,int  size,Stone[][] goban,Player player,boolean ko){
		int index; 
		int index2;
		int capture=0;
		for (index=0 ; index < size ; index++){
			for(index2=0 ; index2 < size ; index2++){
				if(goban[index][index2] != null && goban[index][index2].isDead()== true ){
					capture++;
					goban[index][index2].setColor("nocolor");
					goban[index][index2].setDead(false);
					type[index][index2]=0;
				}
			}
		}
		if(ko==false){
		player.setCapture(capture + player.getCapture());
		}
	}
	/**
	 *remove deadcheck if the chainkill failed
	 *@param goban
	 *@param size is the goban size 
	 */
	public void Undead (Stone[][] goban, int size){
		int index; 
		int index2;
		for (index=0 ; index < size ; index++){
			for(index2=0 ; index2 < size ; index2++){
				if(goban[index][index2] != null && goban[index][index2].isDead()== true ){
					goban[index][index2].setDead(false);
				}
			}
		}
	}
	/**
	 * check if they are some stone to catch
	 * @param type  is the type of case  
	 * @param size is the goban size
	 * @param stone is the stone put on the goban
	 * @param goban is the goban 
	 */
	public void Impact (Stone stone,Goban goban,Player player,boolean ko){
	//case x+1
	if(stone.getAxisX()<goban.getSize()-1){	
		Stone nextstone = goban.getGoban()[stone.getAxisX()+1][stone.getAxisY()];
		if(stone.getColor().equals(goban.getGoban()[nextstone.getAxisX()][nextstone.getAxisY()].getColor())==false && nextstone.getColor().equals("nocolor")==false ){
			Catch(nextstone, goban,player, ko);
			}
		}	
	//case y+1
	if(stone.getAxisY()< goban.getSize()-1){	
		Stone nextstone = goban.getGoban()[stone.getAxisX()][stone.getAxisY()+1];
		if(stone.getColor().equals(goban.getGoban()[nextstone.getAxisX()][nextstone.getAxisY()].getColor())==false && nextstone.getColor().equals("nocolor")==false){
			Catch(nextstone, goban,player, ko);
			}
		}	
	//case x-1
	if(stone.getAxisX() > 0 ){	
		Stone nextstone = goban.getGoban()[stone.getAxisX()-1][stone.getAxisY()];
		if(stone.getColor().equals(goban.getGoban()[nextstone.getAxisX()][nextstone.getAxisY()].getColor())==false  && nextstone.getColor().equals("nocolor")==false){
			Catch(nextstone, goban,player, ko);
			}
		}	
	//case y-1
	if(stone.getAxisY() > 0){
		Stone nextstone = goban.getGoban()[stone.getAxisX()][stone.getAxisY()-1];
		if(stone.getColor().equals(goban.getGoban()[stone.getAxisX()][stone.getAxisY()-1].getColor())==false && nextstone.getColor().equals("nocolor")==false){
			Catch(nextstone, goban,player, ko);
			}
		}	
		goban.memorize(goban.getHistoric(), goban);
	}
	/**
	 * action for catch stone
	 * @param stone the stone played
	 * @param goban is the goban
	 * @param player the player who played a stone
	 * @param ko boolean who said if is a simulation
	 */
	public void Catch(Stone stone,Goban goban,Player player,boolean ko){
		boolean[][] marqueur = new boolean[goban.getSize()][goban.getSize()];
		Initmarqueur(marqueur,goban.getSize());
		if(Dead(goban.getGoban(),goban.getType(),stone.getAxisX(),stone.getAxisY(),goban.getSize())==true){
			if(Deathchain(goban.getType(), stone, goban.getSize(), goban.getGoban(), marqueur)==true){
			Destroy(goban.getType(),goban.getSize(), goban.getGoban(),player, ko);
			}
			else{
				Undead(goban.getGoban(),goban.getSize());
				}	
			}
		}
}

