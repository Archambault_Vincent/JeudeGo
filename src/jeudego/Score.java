package jeudego;

import Data.Player;
import Data.Stone;

/**
 * @author Vincent Archambault
 *
 */
public class Score {
	/**
	 * initialization marqueur 
	 * @param marqueur
	 * @param size of the goban
	 */
	public void Initmarqueur(boolean[][] marqueur,int size){
		int index;
		int index2;
		for(index=0 ; index < size-1 ; index++){
			for(index2=0 ; index2 < size-1 ; index2++){
			marqueur[index][index2]=false;
			}
		}
	}
	/**
	 * calculate final Score 
	 * @param type type of the case
	 * @param player the first player
	 * @param player2 the second player
	 * @param size size of the goban
	 */
	public void Finalscore(int[][] type,Player player,Player player2,int size ){
		float komi=(float) 7.5;
		float score1=0;
		float score2=0;
		int index;
		int index2;
		int valor;
		for(index=0 ; index < size ; index++){
			for(index2=0 ; index2 < size ; index2++){
				//for each case add one point too the player 
				valor=type[index][index2];
				switch (valor){
				case 1 : 
					score1=score1+1;
					break;
				case 2 : 
					score2=score2+1;
					break;
				case -1:
					score1=score1+1;
					break;
				case -2:
					score2=score2+1;
					break;
				}
			}
		}
		player.setScore(score1+player.getHandicap());
		player2.setScore(score2+komi+player2.getHandicap());
	}
	/**
	 *Search a Stone for create a Territory 
	 * @param goban table of stone
	 * @param stone the stone 
	 * @param type table of type 
	 * @param size size of the goban
	 * @param marqueur say if we have already see the stone 
	 */
	public void SearchTerritory(Stone[][] goban,Stone stone,int[][] type,int size,boolean[][] marqueur){
		//say we visit this stone 
		marqueur[stone.getAxisX()][stone.getAxisY()]=true;
		//case x+1
		if(stone.getAxisX()<size-1){	
			if(marqueur[stone.getAxisX()+1][stone.getAxisY()]==false ){
			Stone stonenext=goban[stone.getAxisX()+1][stone.getAxisY()];
			//if the case is empty try create a territory
			if(type[stone.getAxisX()+1][stone.getAxisY()] == 0){
				Territory(goban, type, size, marqueur,stonenext,stone.getColor());	
			}
			else{
				SearchTerritory(goban,stonenext, type, size, marqueur);
				}
			}	
		}
			//case y+1
		if(stone.getAxisY()< size-1){	
			if(marqueur[stone.getAxisX()][stone.getAxisY()+1]==false ) {
				Stone stonenext=goban[stone.getAxisX()][stone.getAxisY()+1];
				//if the case is empty try create a territory
				if(type[stone.getAxisX()][stone.getAxisY()+1] == 0 ){
					Territory(goban, type, size, marqueur,stonenext,stone.getColor());	
				}
				else{
					SearchTerritory(goban,stonenext, type, size, marqueur);
				}
			}
		}	
		//case x-1
		if(stone.getAxisX() > 0 ){	
			if(marqueur[stone.getAxisX()-1][stone.getAxisY()]==false ){
				Stone stonenext=goban[stone.getAxisX()-1][stone.getAxisY()];
				//if the case is empty try create a territory
				if(type[stone.getAxisX()-1][stone.getAxisY()] == 0 ){
					Territory(goban, type, size, marqueur,stonenext,stone.getColor());	
				}
				else{
					SearchTerritory(goban,stonenext, type, size, marqueur);
				}
			}	
		}	
		//case y-1
		if(stone.getAxisY() > 0){
				if(marqueur[stone.getAxisX()][stone.getAxisY()-1]==false ){
			Stone stonenext=goban[stone.getAxisX()][stone.getAxisY()-1];
			//if the case is empty try create a territory
			if(type[stone.getAxisX()][stone.getAxisY()-1] == 0 ){
				Territory(goban, type, size, marqueur,stonenext,stone.getColor());	
					}
				else{
					SearchTerritory(goban,goban[stone.getAxisX()][stone.getAxisY()-1], type, size, marqueur);
					}
				}	
			}
		} 
	/**
	 * create a Territory
	 * @param goban a table of stone
	 * @param type table of type 
	 * @param size size of the goban
	 * @param marqueur say if have already see the stone
	 * @param stone case where we visite 
	 * @param colors colors of the territory create
	 */
	public void Territory(Stone[][] goban,int[][] type,int size,boolean[][] marqueur,Stone stone,String colors){
		int territory;
		//say we visit this stone
		marqueur[stone.getAxisX()][stone.getAxisY()]=true;
		//check the color for create territory  
		if(colors.equals("black")){
			territory=-1;
		}
		else{
			territory=-2;
		}
		type[stone.getAxisX()][stone.getAxisY()]=territory;
		//check stone at x+1
		if(stone.getAxisX() < size-1){
			//case type 0
			if(type[stone.getAxisX()+1][stone.getAxisY()]==0){
				if(marqueur[stone.getAxisX()+1][stone.getAxisY()]==false){
					Stone stonenext = goban[stone.getAxisX()+1][stone.getAxisY()];
					Territory(goban, type, size, marqueur, stonenext, colors);
				}
				else{
					DestroyTerritory(goban, type, size, marqueur, stone, territory);
				}
			}
			//case type=1
			if(type[stone.getAxisX()+1][stone.getAxisY()]==1 || type[stone.getAxisX()+1][stone.getAxisY()]==-1){
				//check the territory type for create or destroy territory
				if(territory==-1){
					if(marqueur[stone.getAxisX()+1][stone.getAxisY()]==false){
						Stone stonenext = goban[stone.getAxisX()+1][stone.getAxisY()];
						SearchTerritory(goban, stonenext, type, size, marqueur);
					}
				}
				else{
					DestroyTerritory(goban, type, size, marqueur, stone, territory);
				}
			}
			//case type=2
			if(type[stone.getAxisX()+1][stone.getAxisY()]==2 || type[stone.getAxisX()+1][stone.getAxisY()]==-2){
				//check the territory type for create or destroy territory
				if(territory==-2){
					if(marqueur[stone.getAxisX()+1][stone.getAxisY()]==false){
					
						Stone stonenext = goban[stone.getAxisX()+1][stone.getAxisY()];
						SearchTerritory(goban, stonenext, type, size, marqueur);
					}
				}
				else{
					DestroyTerritory(goban, type, size, marqueur, stone, territory);
				}
			}
			
		} 
		//check stone y+1
		if(stone.getAxisY()<size-1){	
			//case type=0
			if(type[stone.getAxisX()][stone.getAxisY()+1]==0){
				if(marqueur[stone.getAxisX()][stone.getAxisY()+1]==false){
					Stone stonenext = goban[stone.getAxisX()][stone.getAxisY()+1];
					Territory(goban, type, size, marqueur, stonenext, colors);
				}
				else{
					DestroyTerritory(goban, type, size, marqueur, stone, territory);
				}
			}
			//case type=1
			if(type[stone.getAxisX()][stone.getAxisY()+1]==1 || type[stone.getAxisX()][stone.getAxisY()+1]==-1){
				//check the territory type for create or destroy territory
				if(territory==-1){
					if(marqueur[stone.getAxisX()][stone.getAxisY()+1]==false){
						Stone stonenext = goban[stone.getAxisX()][stone.getAxisY()+1];
						SearchTerritory(goban, stonenext, type, size, marqueur);
					}
				}
				else{
					DestroyTerritory(goban, type, size, marqueur, stone, territory);
				}
			}
			if(type[stone.getAxisX()][stone.getAxisY()+1]==2 || type[stone.getAxisX()][stone.getAxisY()+1]==-2){
				//check the territory type for create or destroy territory
				if(territory==-2){
					if(marqueur[stone.getAxisX()][stone.getAxisY()+1]==false){
						Stone stonenext = goban[stone.getAxisX()][stone.getAxisY()+1];
						SearchTerritory(goban, stonenext, type, size, marqueur);
					}
				}
				else{
					DestroyTerritory(goban, type, size, marqueur, stone, territory);
				}
			}
			
	}
	//check stone x-1
	if(stone.getAxisX() > 0){	
		//case type=0
		if(type[stone.getAxisX()-1][stone.getAxisY()]==0){
			if(marqueur[stone.getAxisX()-1][stone.getAxisY()]==false){
				Stone stonenext = goban[stone.getAxisX()-1][stone.getAxisY()];
				Territory(goban, type, size, marqueur, stonenext, colors);
			}
			else{
				DestroyTerritory(goban, type, size, marqueur, stone, territory);
			}
		}
		//case type=1
		if(type[stone.getAxisX()-1][stone.getAxisY()]==1 || type[stone.getAxisX()-1][stone.getAxisY()]==-1){
			//check the territory type for create or destroy territory
			if(territory==-1){
				if(marqueur[stone.getAxisX()-1][stone.getAxisY()]==false){
					Stone stonenext = goban[stone.getAxisX()-1][stone.getAxisY()];
					SearchTerritory(goban, stonenext, type, size, marqueur);
				}
			}
			else{
				DestroyTerritory(goban, type, size, marqueur, stone, territory);
			}
		}
		if(type[stone.getAxisX()-1][stone.getAxisY()]==2 || type[stone.getAxisX()-1][stone.getAxisY()]==-2){
			//check the territory type for create or destroy territory
			if(territory==-2){
				if(marqueur[stone.getAxisX()-1][stone.getAxisY()]==false){
					Stone stonenext = goban[stone.getAxisX()-1][stone.getAxisY()];
					SearchTerritory(goban, stonenext, type, size, marqueur);
				}
			}
			else{
				DestroyTerritory(goban, type, size, marqueur, stone, territory);
			}
		}
		
		}
	//check stone y-1
	if(stone.getAxisY() > 0){	
		//case type=0
		if(type[stone.getAxisX()][stone.getAxisY()-1]==0){
			if(marqueur[stone.getAxisX()][stone.getAxisY()-1]==false){
				Stone stonenext = goban[stone.getAxisX()][stone.getAxisY()-1];
				Territory(goban, type, size, marqueur, stonenext, colors);
			}
			else{
				DestroyTerritory(goban, type, size, marqueur, stone, territory);
			}
		}
		//case type=1
		if(type[stone.getAxisX()][stone.getAxisY()-1]==1 || type[stone.getAxisX()][stone.getAxisY()-1]==-1){
			//check the territory type for create or destroy territory
			if(territory==-1){
				if(marqueur[stone.getAxisX()][stone.getAxisY()-1]==false){
				
					Stone stonenext = goban[stone.getAxisX()][stone.getAxisY()-1];
					SearchTerritory(goban, stonenext, type, size, marqueur);
				}
			}
			else{
				DestroyTerritory(goban, type, size, marqueur, stone, territory);
			}
		}
		//case type=1
		if(type[stone.getAxisX()][stone.getAxisY()-1]==2 || type[stone.getAxisX()][stone.getAxisY()-1]==-2){
			//check the territory type for create or destroy territory
			if(territory==-2){
				if(marqueur[stone.getAxisX()][stone.getAxisY()-1]==false){
				
					Stone stonenext = goban[stone.getAxisX()][stone.getAxisY()-1];
					SearchTerritory(goban, stonenext, type, size, marqueur);
				}
			}
			else{
				DestroyTerritory(goban, type, size, marqueur, stone, territory);
				}
			}
		}	
	}
	/**
	 *search one stone  for launch  searchTerritory
	 * @param size size of the goban
	 * @param goban table of stone
	 * @param type table of type
	 */
	public void firstStone(int size , Stone[][] goban , int[][] type){
		int index;
		int index2;
		boolean[][] marqueur= new boolean[size][size];
		//initialization  marqueur 
		Initmarqueur(marqueur, size);
		for(index=0 ; index < size-1 ; index++){
			for(index2=0 ; index2 < size-1 ; index2++){
			if(type[index][index2] != 0){
				SearchTerritory(goban,goban[index][index2], type, size, marqueur);
				}
			}
		}
		
	}
	/**
	 * Destroy a Territory create 
	 * @param goban the table of stone
	 * @param type table of type
	 * @param size size of the goban
	 * @param marqueur say if have already visit this stone
	 * @param stone the stone who begin this function
	 * @param territory the territory destroy
	 */
	public void DestroyTerritory(Stone[][] goban,int[][] type,int size,boolean[][] marqueur,Stone stone,int territory){
				//destroy the territory 
				type[stone.getAxisX()][stone.getAxisY()]=0;
				//case x+1
				if(stone.getAxisX()<size-1){	
					if(type[stone.getAxisX()+1][stone.getAxisY()] == territory){
						Stone stonenext = goban[stone.getAxisX()+1][stone.getAxisY()];
						DestroyTerritory(goban, type, size, marqueur, stonenext, territory);
					}
				}	
				//case y+1
				if(stone.getAxisY()< size-1){	
					//search a territory 
					if(type[stone.getAxisX()][stone.getAxisY()+1] == territory){
						Stone stonenext = goban[stone.getAxisX()][stone.getAxisY()+1];
						DestroyTerritory(goban, type, size, marqueur, stonenext, territory);
					}
				}
				
				//case x-1
				if(stone.getAxisX() > 0 ){	
					//search a territory
					if(type[stone.getAxisX()-1][stone.getAxisY()] == territory){
						Stone stonenext = goban[stone.getAxisX()-1][stone.getAxisY()];
						DestroyTerritory(goban, type, size, marqueur, stonenext, territory);
					}
				}	
				//case y-1
				if(stone.getAxisY() > 0){
					if(type[stone.getAxisX()][stone.getAxisY()-1] == territory){
						Stone stonenext = goban[stone.getAxisX()][stone.getAxisY()-1];
						DestroyTerritory(goban, type, size, marqueur, stonenext, territory);
						}
				}
		}
}				
