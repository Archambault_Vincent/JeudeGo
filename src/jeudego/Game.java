/**
 * @author Vincent Archambault
 */
package jeudego;

import Data.Player;
import Data.Stone;

public class Game {
/**
 * Manages Turn  of player
 * @param player
 * @param player2
 * @param goban
 * @param stone
 */
public void Turn (Player player,Player player2,Goban goban,Stone stone){
	if(goban.Full(goban.getType(),goban.getSize())==false && (player.getPass()+player2.getPass()<2)){
		//check if the player is human for play the move
		if(player.isTurn()==true){
			if(player.isHuman()==true){
				HumanTurn(player, player2, goban, stone);
				goban.setTurn(goban.getTurn()+1);
			}	
				else{
					Iaturn(player, player2, goban);
				}
			}
		else{
			if(player2.isHuman()==true){
				HumanTurn(player, player2, goban, stone);
				goban.setTurn(goban.getTurn()+1);
			}	
				else{
					Iaturn(player, player2, goban);
				}
			}
		}
		else{
			Score score = new Score();
			score.firstStone(goban.getSize(), goban.getGoban(),goban.getType());
			score.Finalscore(goban.getType(), player, player2,goban.getSize());
		}
	}
/**
 * manages the ia turn
 * @param player the player who played 
 * @param player2 the 2nd player
 * @param goban 
 */
public void Iaturn(Player player,Player player2,Goban goban){
	Move move=new Move();
	Ia ia=new Ia();
	//choose if is a start move 
	if(goban.getTurn()<4){
		// check if ia is the player 
		if(player.isTurn()==true){
		Stone stoneplay=ia.FirstMove(goban, player);
		goban.putStone(goban,stoneplay);
		goban.assignType(stoneplay,goban.getType());
		move.Impact(stoneplay, goban, player, false);
		player.setTurn(false);
		player2.setTurn(true);
		goban.setTurn(goban.getTurn()+1);
		}
		else{
			Stone stoneplay=ia.FirstMove(goban, player2);
			goban.putStone(goban,stoneplay);
			goban.assignType(stoneplay,goban.getType());
			move.Impact(stoneplay, goban, player2, false);
			player2.setTurn(false);
			player.setTurn(true);
			goban.setTurn(goban.getTurn()+1);
		}
	}
	else{
		// check if ia is the player 
		if(player.isTurn()==true){
		ia.cornerstart(goban, player2,player);
		player.setTurn(false);
		player2.setTurn(true);
		goban.setTurn(goban.getTurn()+1);
		}
		else{
			ia.cornerstart(goban, player,player2);
			player2.setTurn(false);
			player.setTurn(true);
			goban.setTurn(goban.getTurn()+1);
		}
	}
}
/**
 * manages the human turn
 * @param player the first player
 * @param player2 the second player
 * @param goban the goban
 * @param stone the stone who played the player
 */
public void HumanTurn (Player player,Player player2,Goban goban,Stone stone){
		Move move= new Move();
		Capture capture = new Capture();
		//check the turn of player
		if((player).isTurn()==true){
			//check if the move can be true 
			if(goban.getType()[stone.getAxisX()][stone.getAxisY()]==0 && move.ko(goban, stone ,player)==true && move.Suicide(stone, goban,player)==true ){
				goban.putStone(goban, stone);
				goban.assignType(stone, goban.getType());
				capture.Impact(stone, goban,player, false);
				player.setTurn(false);
				player2.setTurn(true);
				player.setPass(0);
				player2.setPass(0);
			}
		}
		else{
			//check if the move can be true 
			if(goban.getType()[stone.getAxisX()][stone.getAxisY()]==0 && move.ko(goban, stone,player2)==true && move.Suicide(stone, goban,player2)==true ){
				goban.putStone(goban, stone);
				goban.assignType(stone, goban.getType());
				capture.Impact(stone, goban,player2, false);
				player2.setTurn(false);
				player.setTurn(true);
				player.setPass(0);
				player2.setPass(0);
			}
		}
	}
}
