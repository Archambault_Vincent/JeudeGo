package jeudego;

import Data.Player;
import Data.Stone;

/**
 * @author Vincent Archambault
 *
 */
public class Move extends Capture {
/**
 * test a suicide case
 * @param stone the stone play
 * @param goban the goban
 * @param player the player who do the move
 * @return false if the move is forbid
 * @return true if the move can be played
 */
	public boolean Suicide(Stone stone,Goban goban,Player player) {
		Stone stonetest =new Stone(player.getColor(),stone.getAxisX(),stone.getAxisY(),false);
		Player playertest=new Player(player.getColor(),player.getName(),player.getScore(),player.isHuman(),player.getHandicap(),player.isTurn());
		boolean[][] marqueur = new boolean[goban.getSize()][goban.getSize()];
		Initmarqueur(marqueur,goban.getSize());
		//create a copy of the goban 
		Goban gobansuicide=new Goban(goban.getSize());
		goban.Copytype(goban,gobansuicide);
		goban.Copygoban(goban, gobansuicide);
		gobansuicide.putStone(gobansuicide, stonetest);
		gobansuicide.assignType(stonetest,gobansuicide.getType());
		//make a chainkill for check if they are some deadstone 
		Impact(stonetest, gobansuicide,playertest, true);
	if(	Dead(gobansuicide.getGoban(),gobansuicide.getType(),stonetest.getAxisX(),stonetest.getAxisY(), gobansuicide.getSize())==true){
		boolean suicide=Deathchain(gobansuicide.getType(), stonetest, gobansuicide.getSize(), gobansuicide.getGoban(), marqueur);
		Undead(goban.getGoban(),goban.getSize());
			if(suicide==true){
				return false;
			}	
		}
		Undead(goban.getGoban(),goban.getSize());	
		return true;
	}
	/**
	 * Test if with this move we make a cycle 
	 * @param goban the goban
	 * @param stone the stone play
	 * @return true if the move is valid
	 *@return false if the move isn't valid
	 */
	public boolean ko (Goban goban,Stone stone,Player player){
	if(goban.getHistoric().size()< 3){	
		return true;
				}
	else{	
		int index;
		int index2;
		Player playertest=new Player(player.getColor(),player.getName(),0,player.isHuman(),0,player.isTurn());
		boolean[][] marqueur = new boolean[goban.getSize()][goban.getSize()];
			Initmarqueur(marqueur,goban.getSize());
			//copy  goban for testing 
			Goban gobantest=new Goban(goban.getSize());
			goban.Copygoban(goban, gobantest);
			goban.Copytype(goban, gobantest);
			//copy goban for rewind 
			Goban gobanhistoric=new Goban(goban.getSize());
			gobanhistoric.Copygoban(goban.getHistoric().get(goban.getHistoric().size()-2),gobanhistoric);
			gobanhistoric.Copytype(goban.getHistoric().get(goban.getHistoric().size()-2),gobanhistoric);
			//Simulation move
			gobantest.putStone(gobantest, stone);
			gobantest.assignType(stone,gobantest.getType());
			Impact(stone, gobantest,playertest, true);
			for (index=0 ;index< goban.getSize() ; index++ ){
				for(index2=0 ; index2 < goban.getSize() ; index2++){
					//check if is the same goban 
					if(gobantest.getType()[index][index2] != gobanhistoric.getType()[index][index2] ){
						return true;
						}
					}
				}
			}	
		return false;
	}
}


