package jeudego;
/**
 * @author Vincent Archambault
 */
import java.util.ArrayList;

import Data.Player;
import Data.Stone;

public class Ia {
	/**
	 * define one of the first move play
	 * @param goban
	 * @param player
	 * @return the stone play
	 */
	public Stone FirstMove(Goban goban,Player player){
		int corner=0;
		int x=0;
		int y=0;
		corner=(int) (Math.random()*4);
		switch (corner){
			case 0 : 
				x=(int) (Math.random()*(3))+2;
				y=(int) (Math.random()*(3))+2;
				if(goban.getType()[x][y]==0){
					Stone stone = new Stone(player.getColor(),x,y,false);
					Move move= new Move();
					if(move.ko(goban, stone, player)==true && move.Suicide(stone, goban, player)==true ){
						return stone;
					}
				}
			break;	
			case 1 : 
				x=(int) (Math.random()*(3))+goban.getSize()-4;
				y=(int) (Math.random()*(3))+2;
				if(goban.getType()[x][y]==0){
					Stone stone = new Stone(player.getColor(),x,y,false);
					Move move= new Move();
					if(move.ko(goban, stone, player)==true && move.Suicide(stone, goban, player)==true){
						return stone;
					}
				}
				break;
			case 2 : 
				x=(int) (Math.random()*(3))+2;
				y=(int) (Math.random()*(3))+goban.getSize()-4;
				if(goban.getType()[x][y]==0){
					Stone stone = new Stone(player.getColor(),x,y,false);
					Move move= new Move();
					if(move.ko(goban, stone, player)==true && move.Suicide(stone, goban, player)==true){
						return stone;
					}
				}
				else{
					return FirstMove(goban, player);
				}
			break;
			case 3 : 
				x=(int) (Math.random()*(3))+goban.getSize()-4;
				y=(int) (Math.random()*(3))+goban.getSize()-4;
				if(goban.getType()[x][y]==0){
					Stone stone = new Stone(player.getColor(),x,y,false);
					Move move= new Move();
					if(move.ko(goban, stone, player)==true && move.Suicide(stone, goban, player)==true ){
						return stone;
					}
				}
			break;
		}
		return FirstMove(goban, player);
		}
	/**
	 * initialize the simulation and play
	 * @param goban
	 * @param player 
	 * @param ia 
	 * @param indexbase is the index minimize of the AxeX
	 * @param indexmax is the index max of the AxeX
	 * @param indexbase2 is the index minimize of the AxeY
	 * @param indexmax2 is the index minimize of the AxeY
	 */
	public void InitMove (Goban goban , Player player , Player ia,int indexbase,int indexmax,int indexbase2,int indexmax2 ){
		Score score =new Score();
		Goban gobaninit=new Goban(goban.getSize());
		goban.Copygoban(goban, gobaninit);
		goban.Copytype(goban, gobaninit);
		score.firstStone(gobaninit.getSize(), gobaninit.getGoban(),gobaninit.getType());
		ArrayList<Stone> play = new ArrayList<Stone>();
		ArrayList<Stone> finalplay = new ArrayList<Stone>();
		Move move =new Move();
		for(int index=indexbase;index<indexmax;index++){
			for(int index2=indexbase2;index2<indexmax2;index2++){
				if(ia.getColor()=="black"){
					if(gobaninit.getType()[index][index2]==0 || gobaninit.getType()[index][index2]==-2){
						if(move.ko(gobaninit,gobaninit.getGoban()[index][index2],ia)==true && move.Suicide(gobaninit.getGoban()[index][index2],gobaninit,ia)==true ){
							//add all case of move possible in this corner
							play.add(goban.getGoban()[index][index2]);
							}
						}
					}		
					else{
						if(gobaninit.getType()[index][index2]==0 || gobaninit.getType()[index][index2]==-1){
							if(move.ko(gobaninit,gobaninit.getGoban()[index][index2],ia)==true && move.Suicide(gobaninit.getGoban()[index][index2],gobaninit,ia)==true ){
								//add all case of move possible in this corner
								play.add(goban.getGoban()[index][index2]);
								}	
							}
						}
					}
				}
		if(play.isEmpty()==false){
				float[] scoremove = new float[play.size()]; 
			for(int index3=0 ;index3 < play.size();index3++){
				Player playersim = new Player(player.getColor(),player.getName(),player.getScore(),player.isHuman(),player.getHandicap(),player.isTurn());
				Player iasim = new Player(ia.getColor(),ia.getName(),player.getScore(),ia.isHuman(),ia.getHandicap(),ia.isTurn());
				Goban gobansimulation = new Goban(goban.getSize());
				goban.Copygoban(goban,gobansimulation);
				goban.Copytype(goban,gobansimulation);
				//start the simulation and put astone on the fake goban
				Stone stoneput=new Stone(iasim.getColor(),play.get(index3).getAxisX(),play.get(index3).getAxisY(),false);
				gobansimulation.putStone(gobansimulation,stoneput);
				gobansimulation.assignType(stoneput,gobansimulation.getType());
				move.Impact(stoneput, gobansimulation, iasim,false);
				gobansimulation.setTurn(gobansimulation.getTurn()+1);
				for(int index4=0;index4 < goban.getHistoric().size() ;index4++){
					//copy the historic for simulation 
					gobansimulation.getHistoric().add(goban.getHistoric().get(index4));
				}
				// add for each move in the arraylist simulate 5 move
				scoremove[index3]=Simulplay(gobansimulation,0, iasim,playersim,0, indexbase, indexmax, indexbase2, indexmax2);
				}
				finalplay.add(play.get(0));
				float bestscore=scoremove[0];
			for(int index5=0 ;index5 < play.size();index5++){
				if(scoremove[index5]>bestscore){
					//if we found a better move clear all move save
					finalplay.clear();
					finalplay.add(play.get(index5));
					bestscore=scoremove[index5];
				}
				else{
					if(scoremove[index5]==bestscore){
						finalplay.add(play.get(index5));
					}
				}
			}
			if(finalplay.isEmpty()==false){
				//play a move in the array finalmove 
				int stoneplay=(int) (Math.random()*finalplay.size());
				Stone stoneput=new Stone(ia.getColor(),finalplay.get(stoneplay).getAxisX(),finalplay.get(stoneplay).getAxisY(),false);
				goban.putStone(goban,stoneput);
				goban.assignType(stoneput,goban.getType());
				move.Impact(stoneput, goban, player, false);
				ia.setPass(0);
				player.setPass(0);
			}
		}
		else{
			//if he can't play in this corner play in another corner
			cornerstart(goban,player, ia);
		}
	}
	/**
	 * Simulate a 5 move 
	 * @param goban
	 * @param turn
	 * @param player
	 * @param ia
	 * @param bestscore the bestscore return after 5 move 
	 * @param indexbase is the index minimize of the AxeX
	 * @param indexmax is the index max of the AxeX
	 * @param indexbase2 is the index minimize of the AxeY
	 * @param indexmax2 is the index minimize of the AxeY
	 * @return the score after 5 move 
	 */
	public float Simulplay(Goban goban,int turn,Player player,Player ia,float bestscore,int indexbase,int indexmax,int indexbase2,int indexmax2){
		ArrayList<Stone> bestplay = new ArrayList<Stone>();
		float delta=0;
		Move move =new Move();
		Score score = new Score();
		if(turn<6){
			bestscore=0;
			for(int index=indexbase;index<indexmax;index++){
				for(int index2=indexbase2;index2<indexmax2;index2++){
					if(move.ko(goban,goban.getGoban()[index][index2],ia)==true && move.Suicide(goban.getGoban()[index][index2],goban,ia)==true && goban.getType()[index][index2]==0  ){
						Stone stoneplay=new Stone(ia.getColor(),index,index2,false);
						//copy the goban for a simulation
						Goban gobansimulation = new Goban(goban.getSize());
						goban.Copygoban(goban,gobansimulation);
						goban.Copytype(goban,gobansimulation);
						goban.putStone(gobansimulation,stoneplay);
						goban.assignType(stoneplay,gobansimulation.getType());
						move.Impact(stoneplay, gobansimulation, ia, false);
						score.firstStone(goban.getSize(), gobansimulation.getGoban(),gobansimulation.getType());
						if(ia.getColor().equals("white")){
							score.Finalscore(gobansimulation.getType(), player,ia,goban.getSize());
							}
						else{
							score.Finalscore(gobansimulation.getType(), ia,player,goban.getSize());
							}
						//compute the bestscore for play the best move
						delta=ia.getScore()-player.getScore();
						if(bestplay.isEmpty()==true){
							bestscore=delta;
							bestplay.add(stoneplay);
						}
						else{
							if(delta > bestscore){
								//clear array if we found a bestmove
								bestplay.clear();
								bestplay.add(stoneplay);
								bestscore=delta;
							}
							else{
							 if( delta == bestscore){
								bestplay.add(stoneplay);
							 }	
							}
						}
					}
				}
			}
			//play one of the bestmove 
			if(bestplay.isEmpty()==false){
				int stoneplay=(int) (Math.random()*bestplay.size());
				goban.putStone(goban,bestplay.get(stoneplay));
				goban.assignType(bestplay.get(stoneplay),goban.getType());
				move.Impact(bestplay.get(stoneplay), goban, ia, false);
				goban.setTurn(goban.getTurn()+1);
			}
			//simulate a new turn 
			turn++;
			return Simulplay(goban, turn,ia,player,bestscore, indexbase, indexmax, indexbase2, indexmax2);
		}
		return bestscore;
	}
	/**
	 * choose the corner where the ia play 
	 * @param goban
	 * @param player
	 * @param ia
	 */
	public void cornerstart (Goban goban,Player player,Player ia){
	Score score =new Score();
	Goban gobaninit=new Goban(goban.getSize());
	goban.Copygoban(goban, gobaninit);
	goban.Copytype(goban, gobaninit);
	score.firstStone(gobaninit.getSize(), gobaninit.getGoban(),gobaninit.getType());
	//define the corner who the Ia play 
	int	corner=(int) (Math.random()*4);	
	ArrayList<Stone> play = new ArrayList<Stone>();
	Move move =new Move();
	//check all case possible in a array list
	for(int index=0;index<goban.getSize();index++){
		for(int index2=0;index2<goban.getSize();index2++){
			if(gobaninit.getType()[index][index2]==0){
				if(move.ko(goban,goban.getGoban()[index][index2],ia)==true && move.Suicide(goban.getGoban()[index][index2],goban,ia)==true){
					play.add(goban.getGoban()[index][index2]);
						}
					}
				}
			}
		if(play.isEmpty()==false){
			int index=0;
			int max=0;
			int index2=0;
			int max2=0;
			switch (corner) {
				case 0:
					//he play one the Up-Left corner of the goban
					index=0;
					max=goban.getSize()/2;
					index2=0;
					max2=goban.getSize()/2;
					InitMove(goban, player, ia, index,max, index2, max2);
					break;
				case 1:
					//he play one the Up-Right corner of the goban
					index=0;
					max=goban.getSize()/2;
					index2=goban.getSize()/2;
					max2=goban.getSize();
					InitMove(goban, player, ia, index,max, index2, max2);
					break;
				case 2:
					//he play one the Down-Left corner of the goban
					index2=0;
					max2=goban.getSize()/2;
					index=goban.getSize()/2;
					max=goban.getSize();
					InitMove(goban, player, ia, index,max, index2, max2);
					break;	
				case 3:
					//he play one the Down-Right corner of the goban
					index=goban.getSize()/2;
					max=goban.getSize();
					index2=goban.getSize()/2;
					max2=goban.getSize();
					InitMove(goban, player, ia, index,max,index2,max2);
					break;	
				default:
				break;
			}
		}
		else{
		//if he can't play he pass is turn
			ia.setPass(1);
		}
	}
}
