package jeudego;
import java.util.ArrayList;
import Data.Stone;
/**
 * @author Vincent Archambault
 *
 */
public class Goban implements Cloneable {
	public void setTurn(int turn) {
		this.turn = turn;
	}
	private int size;
	private int turn=0;
	private Stone[][] goban;
	private int[][] type; 	
	private ArrayList<Goban> historic = new ArrayList<Goban>(); ;	
	public int getTurn() {
		return turn;
	}
	public Goban(int size){
		this.size=size;
		goban=new Stone[size][size];
		type=new int[size][size];
		for(int index=0; index<size ; index++){
			for(int index2=0;index2<size;index2++){
				type[index][index2]=0;
				goban[index][index2]=new Stone("nocolor",index,index2,false);
			}
		}
	}	
	public Stone[][] getGoban() {
		return goban;
	}
	public int[][] getType() {
		return type;
	}
	/**
	 * put a Stone on the goban 
	 * save the move
	 * increase number of turn 
	 * @param goban
	 * @param stone
	 */
	public void putStone(Goban goban,Stone stone){
		goban.getGoban()[stone.getAxisX()][stone.getAxisY()]=stone;
		}
	public Goban Lastmove(Goban goban){
		return goban.historic.get(turn-1);
				}
	/**
	 * rewind a movement
	 * @param goban
	 * @param x
	 * @param y
	 */
	public boolean Rewind(Goban goban){
		if(historic.size()-1 > 0){
		goban.historic.remove(historic.size()-1);
		goban.setTurn(goban.getTurn()-1);
		goban.Copygoban(goban.historic.get(historic.size()-1),goban);
		goban.Copytype(goban.historic.get(historic.size()-1),goban);
		return true;
		}
		return false;
	}
	/**
	 * remove a stone 
	 * @param goban
	 * @param x
	 * @param y
	 */
	public void removeStone(Stone[][] goban,int x,int y){
		Stone empty = new Stone("nocolor",x,y,false);
		goban[x][y]= empty;
	}
	/**
	 * assign a type on the goban 
	 * @param stone
	 * @param type
	  */
	public void assignType(Stone stone,int[][] type){
			if (stone.getColor().equals("black")){
				type[stone.getAxisX()][stone.getAxisY()]=1;
			}
			else {
				type[stone.getAxisX()][stone.getAxisY()]=2;
			}
		}
	/**
	 * remove a element with the rewind 
	 * @param stone
	 * @param type
	 */
	public void removeType(Stone stone,int[][] type ){
	type[stone.getAxisX()][stone.getAxisY()]=0;	
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public Object clone() {
		Object copy = null;
		try {
			copy = super.clone();
		} catch(CloneNotSupportedException cnse) {
			cnse.printStackTrace(System.err);
		}
		return copy;
	}
	/**
	 * add a move in the historic
	 * @param historic
	 * @param goban
	 */
	public void memorize(ArrayList<Goban> historic,Goban goban){
	 Goban gobanhistoric=new Goban(goban.getSize());
	 goban.Copygoban(goban, gobanhistoric);
	 goban.Copytype(goban,gobanhistoric);	
	 historic.add(gobanhistoric);
	}
	
	public ArrayList<Goban> getHistoric() {
		return historic;
	}
	/**
	 * check if the goban is full
	 * @param type
	 * @param size
	 * @return true if the goban is full 
	 *else return false
	 */
	public boolean Full(int[][] type,int size){
		int index;
		int index2;
		for(index=0;index <size;index++ ){
			for(index2=0;index2<size ; index2++){
				if(type[index][index2]==0){
					return false;
				}
			}
		}
		return true;
	}
	/**
	 * Copy the goban 
	 * @param goban the original goban
	 * @param gobancopy the copy of the first goban
	 */
	public void Copygoban (Goban goban,Goban gobancopy){
		int index;
		int index2;
		for (index=0 ;index< goban.getSize() ; index++ ){
			for(index2=0 ; index2 < goban.getSize() ; index2++){
			gobancopy.getGoban()[index][index2]=(Stone) goban.getGoban()[index][index2].clone();
			}
		}	
		gobancopy.setTurn(goban.getTurn());
	}
	/**
	 * Copy the type of the goban
	 * @param goban the original goban
	 * @param gobancopy the copy of the first goban
	 */
	public void Copytype(Goban goban,Goban gobancopy){
		int index;
		int index2;
		for (index=0 ;index< goban.getSize() ; index++ ){
			for(index2=0 ; index2 < goban.getSize() ; index2++){
			gobancopy.getType()[index][index2]=goban.getType()[index][index2];
			}
		}	
	}
}

