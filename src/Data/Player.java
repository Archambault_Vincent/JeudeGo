package Data;
/**
 * @author Vincent
 *
 */
public class Player {
private String color;
private String name;
private float score=0;
private boolean human;
private int handicap=0;
private boolean turn;
private int pass=0;
private int capture=0;
public Player (String color, String name, float score , boolean human,int handicap,boolean turn){
this.color=color;
this.name=name;
this.score=score;
this.human=human;
this.handicap=handicap;
this.turn=turn;
}
public boolean isTurn() {
	return turn;
}
public void setTurn(boolean turn) {
	this.turn = turn;
}
public String getColor() {
	return color;
}
public int getHandicap() {
	return handicap;
}
public void setHandicap(int handicap) {
	this.handicap = handicap;
}
public String getName() {
	return name;
}
public void setScore(float score) {
	this.score = score;
}
public float getScore() {
	return score;
}
public boolean isHuman() {
	return human;
}
public int getPass() {
	return pass;
}
public void setPass(int pass) {
	this.pass = pass;
}
public int getCapture() {
	return capture;
}
public void setCapture(int capture) {
	this.capture = capture;
}
}

